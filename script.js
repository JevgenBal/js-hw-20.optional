function filterCollection(collection, keywords, matchAll, ...fields) {

  const keywordArray = keywords.toLowerCase().split(' ');
  
  const filteredCollection = collection.filter(item => {

    function searchForKeywords(obj) {
      for (const key in obj) {
        if (fields.includes(key)) {

          const fieldValue = obj[key];
          if (Array.isArray(fieldValue)) {

            for (const item of fieldValue) {
              if (searchForKeywords(item)) {
                return true;
              }
            }
          } else if (typeof fieldValue === 'object' && fieldValue !== null) {

            if (searchForKeywords(fieldValue)) {
              return true;
            }
          } else if (typeof fieldValue === 'string' && keywordArray.some(keyword => fieldValue.toLowerCase().includes(keyword))) {

            return true;
          }
        } else if (typeof obj[key] === 'object' && obj[key] !== null) {


          if (searchForKeywords(obj[key])) {
            return true;
          }
        }
      }

      return matchAll ? keywordArray.every(keyword => JSON.stringify(obj).toLowerCase().includes(keyword)) : keywordArray.some(keyword => JSON.stringify(obj).toLowerCase().includes(keyword));
    }
    return searchForKeywords(item);
  });

  return filteredCollection;
}
